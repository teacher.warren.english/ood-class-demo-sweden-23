﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverDemo
{
    public class Zoologist : IObserver
    {
        public void Notified()
        {
            Console.WriteLine("I just saw this animal do something!");
        }
    }
}
