﻿using ObserverDemo;

IObservable jaguar = new Animal();

IObserver warren = new Zoologist();
IObserver dean = new Zoologist();
IObserver reza = new Zoologist();

jaguar.Subscribe(warren);
jaguar.Subscribe(dean);
jaguar.Subscribe(reza);

jaguar.Notify();

jaguar.Unsubscribe(warren);

jaguar.Notify();