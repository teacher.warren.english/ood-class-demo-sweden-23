# OOD Demo

A demonstration of good OO design using the observer pattern, which conforms to many of the SOLID principals. For the Sweden delivery August 2023.

©️ Noroff Accelerate

## Installation

Clone the repository by running the command:
```bash
git clone https://gitlab.com/teacher.warren.english/ood-class-demo-sweden-23.git
```

## Contributing

@teacher.warren.english

## License

[MIT](https://choosealicense.com/licenses/mit/)

Demonstrating good OO design using the observer pattern, which conforms to many of the SOLID principals.
