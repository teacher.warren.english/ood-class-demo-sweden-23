﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverDemo
{
    public class Animal : IObservable
    {
        public List<IObserver> Observers { get; set; }

        public Animal()
        {
            Observers = new List<IObserver>();
        }

        public void Notify()
        {
            Console.WriteLine("Notifying all observers...");

            foreach (var observer in Observers)
            {
                observer.Notified();
            }
        }

        public void Subscribe(IObserver observer)
        {
            Observers.Add(observer);
        }

        public void Unsubscribe(IObserver observer)
        {
            Observers.Remove(observer);
        }
    }
}
